import socket
import time
import json


def envia_identificador(nome):
    pacote = dict(type='id', data=nome)
    linha_pacote = json.dumps(pacote).encode()
    sock.sendto(linha_pacote, (ip, port))

def envia_mensagem(mensagem):
    pacote = dict(type='msg', data=mensagem)
    linha_pacote = json.dumps(pacote).encode()
    sock.sendto(linha_pacote, (ip, port))


ip = '10.64.255.255'
port = 37020

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)


while True:
    message = input('Escreva aqui sua mensagem: ')

    envia_identificador('Samuel Licorio Leiva')
    envia_mensagem(message)

sock.close()
