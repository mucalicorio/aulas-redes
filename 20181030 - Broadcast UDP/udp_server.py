import socket
import json


def divide_pacote(pacote, address):
    pacote_dividido = json.loads(pacote)
    divisoes[pacote_dividido['type']](pacote_dividido, address)

def recebe_id(pacote_dividido, address):
    usuarios_conhecidos[address[0]] = pacote_dividido['data']

def recebe_msg(pacote_dividido, address):
    usuario = usuarios_conhecidos.get(address[0], address[0])
    print ('%s disse: %s' % (usuario, pacote_dividido['data']))

ip = ''
port = 37020

divisoes = {
    'id': recebe_id,
    'msg': recebe_msg,
}

usuarios_conhecidos = {}

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind ((ip, port))

while True:
    try:
        data, address = sock.recvfrom(1024)

        divide_pacote(data, address)
    
    except Exception as e:
        print ('Error: ', e)

sock.close()
