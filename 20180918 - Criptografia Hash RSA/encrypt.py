# coding: utf-8

import uuid
import hashlib
import rsa

def verificar_senha(senha1, senha2):
    return senha1 == senha2

def encripta_rsa(mensagem, chave):
    return rsa.encrypt(mensagem, chave)

(chave_publica, chave_privada) = rsa.newkeys(1024)
tamanho_bloco = 7000

cripto_hash1 = hashlib.sha256()
with open('arquivo1.txt', 'rb') as arquivo1:
    letra1 = arquivo1.read(tamanho_bloco)
    while len(letra1) > 0:
        cripto_hash1.update(letra1)
        mensagem1 = letra1
        letra1 = arquivo1.read(tamanho_bloco)

cripto_hash2 = hashlib.sha256()
with open('arquivo2.txt', 'rb') as arquivo2:
    letra2 = arquivo2.read(tamanho_bloco)
    while len(letra2) > 0:
        cripto_hash2.update(letra2)
        mensagem2 = letra2
        letra2 = arquivo2.read(tamanho_bloco)


ch1 = cripto_hash1.hexdigest()
ch2 = cripto_hash2.hexdigest()
print ('Mensagem (hash): ' + ch1)


mensagem_rsa1 = encripta_rsa(mensagem1, chave_publica)
mensagem_rsa1_str = str(mensagem_rsa1)
mensagem_rsa2 = encripta_rsa(mensagem2, chave_publica)

print ('A mensagem (RSA) é: ' + mensagem_rsa1_str)

mensagem_dec1 = rsa.decrypt(mensagem_rsa1, chave_privada)
mensagem_dec2 = rsa.decrypt(mensagem_rsa2, chave_privada)

print ('A decriptografia de RSA é:\n' + '-'*50 + '\n' + mensagem_dec1.decode('utf8') + '\n' + '-'*50)

if verificar_senha(ch1, ch2):
    print ('Mensagens correspondentes!')
else:
    print ('Mensagens não correspondentes!')

arquivo1.close()
arquivo2.close()
