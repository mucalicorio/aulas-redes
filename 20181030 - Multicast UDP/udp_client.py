import socket
import struct
import json


def envia_identificador(nome):
    pacote = dict(type='id', data=nome)
    linha_pacote = json.dumps(pacote).encode()
    sock.sendto(linha_pacote, grupo_do_zapi)

def envia_mensagem(mensagem):
    pacote = dict(type='msg', data=mensagem)
    linha_pacote = json.dumps(pacote).encode()
    sock.sendto(linha_pacote, grupo_do_zapi)


ip = '224.0.0.10'
port = 10000
grupo_do_zapi = (ip, port)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
ttl = struct.pack('b', 1)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

while True:
    message = input('Escreva aqui sua mensagem: ')

    envia_identificador('Samuel Licorio Leiva')
    envia_mensagem(message)

sock.close()
