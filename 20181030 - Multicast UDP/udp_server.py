import socket
import struct
import json


def divide_pacote(pacote, address):
    pacote_dividido = json.loads(pacote)
    divisoes[pacote_dividido['type']](pacote_dividido, address)

def recebe_id(pacote_dividido, address):
    usuarios_conhecidos[address[0]] = pacote_dividido['data']

def recebe_msg(pacote_dividido, address):
    usuario = usuarios_conhecidos.get(address[0], address[0])
    print ('%s disse: %s' % (usuario, pacote_dividido['data']))

multicast_group = '224.0.0.10'
server_address = ''
port = 10000
grupo_do_zapi = (server_address, port)

divisoes = {
    'id': recebe_id,
    'msg': recebe_msg,
}

usuarios_conhecidos = {}

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind (grupo_do_zapi)

group = socket.inet_aton(multicast_group)
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

while True:
    try:
        data, address = sock.recvfrom(1024)

        divide_pacote(data, address)
    
    except Exception as e:
        print ('Error: ', e)

sock.close()
