# encoding: utf-8

import socket


ip = '127.0.0.1'
port = 8000

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind ((ip, port))

while True:
    data, address = sock.recvfrom(1024)

    data = data.decode()
    print ('Recieved message: ', data)

    dataUpper = data.upper()

    sock.sendto(dataUpper.encode(), (address))

sock.close()
