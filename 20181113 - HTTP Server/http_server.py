import socket
from threading import Thread

server_socket = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM,
)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

server_socket.bind(('0.0.0.0', 8000))

server_socket.listen(1)

def accept_http(client_address, client_socket):
    print(client_address, client_socket)
    data = client_socket.recv(1024)
    print(data)
    content = '''<h1>Parara Timbum!</h1>'''
    response = '''HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\n\nContent-Length: %d \r\n\r\n%s\r\n''' % (len(content), content)
    client_socket.send(response)


while True:
    client_socket, client_address = server_socket.accept()
    thread = Thread(target=accept_http, args=(client_address, client_socket)))
    thread.start()
