# coding: utf-8

class CamadaA:
    def main(self, tipo, texto):
        self.texto = texto
        print ('Tipo: ' + tipo + ' | Texto: ' + texto)
        return texto

    def __init__(self, tipo, texto):
        self.tipo = tipo
        self.texto = texto
        tipo = tipo.lower()

        if 'get' in tipo:
            self.main(tipo, texto)
        elif 'set' in tipo:
            self.main(tipo, texto)

        else:
            raise ('Entrada incorreta!')

    
class CamadaB:
    def main(self, tipo, texto):
        self.texto = texto
        texto_reversed = ''
        for i in reversed(texto):
            texto_reversed += i
        del texto
        print ('Tipo: ' + tipo + ' | Texto Reverso: ' + texto_reversed)
        return texto_reversed

    def __init__(self, tipo, texto):
        self.tipo = tipo
        self.texto = texto
        tipo = tipo.lower()

        if 'get' in tipo:
            self.main(tipo, texto)
        elif 'set' in tipo:
            self.main(tipo, texto)

        else:
            raise('Entrada incorreta!')

class CamadaC:
    def main1(self, tipo, texto):
        self.texto = texto
        palavra_separada = []
        for letra in texto:
            palavra_separada += letra
        palavra_separada = str(palavra_separada)
        print ('Tipo: ' + tipo + ' | Palavra Separada: ' + palavra_separada)
        return palavra_separada

    def main2(self, tipo, texto):
        self.texto = texto
        palavra = ''.join(texto)
        del texto
        print ('Tipo: ' + tipo + ' | Palavra: ' + palavra)
        return palavra

    def __init__(self, tipo, texto):
        self.tipo = tipo
        self.texto = texto
        tipo = tipo.lower()

        if 'get' in tipo:
            self.main1(tipo, texto)
        elif 'set' in tipo:
            self.main2(tipo, texto)

        else:
            raise('Entrada incorreta!')

lista = ['t', 'e', 'x', 't', 'o']
CamadaC('get', lista)
