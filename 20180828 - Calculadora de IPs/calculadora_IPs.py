# encoding: utf-8


def transformaInt(mask):
    maskInt = []
    for i in mask:
        maskInt.append(int(i))
    return maskInt

def transformaHex(maskInt):
    maskHex = []
    for i in range(len(maskInt)):
        maskHex.append('{:x}'.format(maskInt[i]))
    return maskHex

def identificaClasse(ip):
    if int(ip[0])<128:
        return "A"
    if int(ip[0])<192:
        return "B"
    if int(ip[0])<224:
        return "C"
    if int(ip[0])<240:
        return "D"
    else:
        return "E"

def contaBits(mask):
    contadorBits = 0
    for i in range(len(mask)):
        binMask = bin(mask[i])[2:]
        for j in range(len(binMask)):
            if binMask[j] == '1':
                contadorBits += 1
    return contadorBits

def showResults(ip, mask, ip_rede, broadcast, quantidadeBits, hexMask, classe):
    print ('-'*10, 'Informações', '-'*10)
    print ('Classe:        %s' % (classe))
    print ('IP:            %s.%s.%s.%s / %s' % (ip[0], ip[1], ip[2], ip[3], quantidadeBits))
    print ('Máscara:       %s.%s.%s.%s' % (mask[0], mask[1], mask[2], mask[3]))
    print ('Hexadecimal:   %s.%s.%s.%s' % (hexMask[0], hexMask[1], hexMask[2], hexMask[3]))
    print ('Rede:          %d.%d.%d.%d' % (ip_rede[0], ip_rede[1], ip_rede[2], ip_rede[3]))
    print ('Broadcast:     %d.%d.%d.%d' % (broadcast[0], broadcast[1], broadcast[2], broadcast[3]))
    print ('-'*33)


ip = input('Entre com o endereço IP: ').split('.')
mask = input('Entre com a Máscara de Rede: ').split('.')

broadcast = []
ip_rede = []

for i in range(len(ip)):
    ip_rede.append(int(ip[i]) & int(mask[i]))
    broadcast.append((~int(mask[i])&0xff) | int(ip_rede[i]))

maskInt = transformaInt(mask)

classe = identificaClasse(ip)
quantidadeBits = contaBits(maskInt)
hexMask = transformaHex(maskInt)
showResults(ip, mask, ip_rede, broadcast, quantidadeBits, hexMask, classe)
