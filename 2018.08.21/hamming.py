import math
from itertools import chain
from functools import reduce

def parityBitsQuantity(word_size):
    return int(math.log(word_size,2))+1

def parityIndexes(parityBitsIndex,word_size):
    for i in range(parityBitsIndex,word_size+1,parityBitsIndex*2):
        yield range(i,min(i+parityBitsIndex,word_size+1))

def isInvalidParity(index,word):
    bitsToCheck = chain(*parityIndexes(index,len(word)))
    bitsValue = map(lambda bit: word[bit-1], bitsToCheck)
    return reduce(lambda a,b : a ^ b, map(bool,map(int,bitsValue)))

def check(word):
    size = len(word)
    paritiesQuantity = parityBitsQuantity(size)
    wrongBit = 0
    for p in range(paritiesQuantity):
        index = 2**p
        if isInvalidParity(index,word):
            wrongBit += index
    if wrongBit == 0:
        print "OK"
    else:
        print wrongBit

check("0011001")


    



#c = 1 0 0 1 0 0 0 0 1 0 0 1
#i = 1 2 3 4 5 6 7 8 9 A B C